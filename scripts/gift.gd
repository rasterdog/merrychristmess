extends Area2D

onready var world = $'../../../World'
onready var player = world.get_node('Player')
onready var sprite = $Sprite
onready var objective_sound = $'/root/Game/StreamPlayer/Objective'

var collected = false

func _on_Gift_body_entered(body):
	if(!collected && body == player):
		collected = true
		sprite.frame = 1
		objective_sound.play()
		world.increment_gifts_count()
