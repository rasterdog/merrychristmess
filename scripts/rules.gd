extends Node

const FORMAT = 'x%d'

onready var gifts_count_label = $'HUD/GiftsCount'

export var total_gifts : int

var gifts_delivered = 0

func _ready():
	gifts_count_label.text = FORMAT % total_gifts

func increment_gifts_count():
	gifts_delivered += 1
	gifts_count_label.text = FORMAT % (total_gifts - gifts_delivered)
