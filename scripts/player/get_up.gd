extends Node

onready var player = $"../../"
onready var sprite = $"../../AnimatedSprite"

export var delay : float

var time = 0

func enter():
	sprite.play('get up')
	time = 0
	
func exit():
	pass

func process():
	pass

func physics_process():
	
	if(time >= delay):
		return 'moving'

	time = time + 1
	
	return null
