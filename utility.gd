extends Node2D

const UP_VECTOR = Vector2(0, -1)
const RIGHT_VECTOR = Vector2(1, 0)
const LEFT_VECTOR = Vector2(-1, 0)

const EPSILON = 0.01

func body_on_wall(body : KinematicBody2D) -> bool:
	
	for i in body.get_slide_count():
		var normal = body.get_slide_collision(i).normal
		if(normal == RIGHT_VECTOR || normal == LEFT_VECTOR):
			return true
	return false

func body_on_slope(body : KinematicBody2D) -> float:
	
	for i in body.get_slide_count():
		var slope = _normal_reveals_slope(body.get_slide_collision(i).normal)
		if(slope):
			return slope
	return 0.0

func _normal_reveals_slope(normal : Vector2) -> float:
	
	var angle = normal.angle_to(UP_VECTOR)
	
	var abs_angle = abs(angle)
	if(abs_angle < PI / 4 + EPSILON && abs_angle > PI / 4 - EPSILON):
		return -sign(angle)
	return 0.0
