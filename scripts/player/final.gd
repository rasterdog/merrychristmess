extends Node

onready var player = $"../../"
onready var sprite = $"../../AnimatedSprite"

func enter():
	sprite.play('fallen')
	player.velocity = Vector2(0, 0)

func exit():
	pass

func process():
	pass

func physics_process():
	return null
