extends CanvasLayer

const FORMAT = 'TIME: %d:%02d\nGIFTS: %d/%d\n\n- PRESS ENTER TO EXIT -'

onready var title_screen = load('res://scenes/TitleScreen.tscn')
onready var menu_camera = $'/root/Game/Camera2D'
onready var menu_sound = $'/root/Game/StreamPlayer/Menu'

onready var stats = $Stats
onready var timer = $'../World/Timer'
onready var world = $'../World'

func _ready():
	stats.text = FORMAT % [timer.minutes, timer.seconds, world.gifts_delivered, world.total_gifts]

func _process(delta):
	
	if(Input.is_action_just_pressed('ui_ok')):
		menu_sound.play()
		_back_to_menu()
	
func _back_to_menu():
	
	$'/root/Game'.add_child(title_screen.instance())
	menu_camera.current = true
	get_parent().remove_child(world)
	get_parent().remove_child(self)