extends Node

onready var player = $"../../"
onready var sprite = $"../../AnimatedSprite"
onready var impact1_sound = $'/root/Game/StreamPlayer/Impact1'
onready var impact2_sound = $'/root/Game/StreamPlayer/Impact2'

export var strength : float
export var reverse_side_strength : float
export var alcohol_influence : float
export var max_velocity : float
export var balance_threshold : float
export var friction : float

onready var portal_input_directions = {
	-1: 'enter_up',
	1: 'enter_down'
}

var idle = true

func enter():
	
	sprite.play('idle')
	player.velocity.y = 100
	
func exit():
	sprite.set_speed_scale(1)

func process():
	
	if(player.velocity.x != 0):
		player.facing_left = player.velocity.x < 0
		sprite.set_flip_h(player.facing_left)
		
		if(idle):
			sprite.play('moving')
			idle = false
		
		sprite.set_speed_scale(abs(player.velocity.x) / 20)
		
	elif(!idle):
		sprite.play('idle')
		idle = true

func physics_process():
	
	if(player.portal_ref && Input.is_action_just_pressed(portal_input_directions[player.portal_ref.portal_direction])):
		player.portal_ref.use()
	
	var abs_horizontal_velocity = abs(player.velocity.x)
	
	if(Utility.body_on_wall(player)):
		if(abs_horizontal_velocity > balance_threshold):
			impact2_sound.play()
			player.velocity.x = -player.velocity.x
			return 'fallen'
		else:
			player.velocity.x = 0
	
	if(Input.is_action_pressed('move_right')):
		player.velocity.x += (strength if player.velocity.x > 0 else reverse_side_strength)
	elif(Input.is_action_pressed('move_left')):
		player.velocity.x -= (strength if player.velocity.x < 0 else reverse_side_strength)
	else:
		if(abs_horizontal_velocity > balance_threshold):
			player.velocity.x += sign(player.velocity.x) * alcohol_influence
		elif(abs(player.velocity.x) >= friction):
			player.velocity.x -= friction * sign(player.velocity.x)
		else:
			player.velocity.x = 0
	
	if(!player.is_on_floor()):
		return 'falling'
	
	if(Utility.body_on_slope(player)):
		return 'climbing'
	
	if(abs_horizontal_velocity > max_velocity):
		impact1_sound.play()
		return 'fallen'
	
	if(Input.is_action_just_pressed("jump")):
		return 'jumping'
	
	return null
