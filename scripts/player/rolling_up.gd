extends Node

onready var player = $"../../"
onready var sprite = $"../../AnimatedSprite"

export var friction : float

var slope : float

func enter():
	
	player.velocity.y = -abs(player.velocity.x) + 1
	slope = -sign(player.velocity.x)
	
func exit():
	sprite.set_speed_scale(1)

func process():
	sprite.set_speed_scale(max(abs(player.velocity.x) / 50, 0.5))

func physics_process():
	
	if(Utility.body_on_slope(player)):
		if(abs(player.velocity.x) <= friction):
			$"../Tumble".slope = slope
			return 'tumble'
		
		player.velocity.x -= friction * sign(player.velocity.x)
		player.velocity.y = -abs(player.velocity.x) + 1
	
	elif(!player.is_on_floor()):
		return 'falling'
	
	return null
