extends CanvasLayer

onready var world = preload('res://scenes/World.tscn')
onready var menu_sound = $'/root/Game/StreamPlayer/Menu'

func _process(delta):
	
	if(Input.is_action_just_pressed('ui_ok')):
		menu_sound.play()
		_start_game()
	
func _start_game():

	$'/root/Game'.add_child(world.instance())
	get_parent().remove_child(self)
