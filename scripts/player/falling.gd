extends Node

onready var player = $"../../"
onready var sprite = $"../../AnimatedSprite"
onready var impact1_sound = $'/root/Game/StreamPlayer/Impact1'
onready var impact2_sound = $'/root/Game/StreamPlayer/Impact2'

export var fall_acceleration : float
export var max_fall_velocity : float

func enter():
	
	if (player.facing_left && player.velocity.x > 0) || (!player.facing_left && player.velocity.x < 0):
		sprite.play('reverse falling')
	else:
		sprite.play('falling')
		
func exit():
	pass

func process():
	pass

func physics_process():
	
	if(Utility.body_on_wall(player)):
		sprite.play('reverse falling')
		impact2_sound.play()
		player.velocity.x = -player.velocity.x / 2
	
	var slope = Utility.body_on_slope(player)
	if(slope):
		$"../Tumble".slope = slope
		return 'tumble'
	
	if(player.is_on_floor()):
		impact1_sound.play()
		return 'fallen'
		
	player.velocity.y += fall_acceleration
	if(player.velocity.y > max_fall_velocity): player.velocity.y = max_fall_velocity
	
	return null
