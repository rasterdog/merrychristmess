extends Node

onready var player = $"../../"
onready var sprite = $"../../AnimatedSprite"
onready var impact2_sound = $'/root/Game/StreamPlayer/Impact2'

export var friction : float

func enter():
	player.velocity.y = 1
	
func exit():
	sprite.set_speed_scale(1)

func process():
	sprite.set_speed_scale(max(abs(player.velocity.x) / 50, 0.5))

func physics_process():
	
	if(Utility.body_on_slope(player)):
		return 'rolling up'
		
	if(!player.is_on_floor()):
		return 'falling'
	
	if(abs(player.velocity.x) <= friction):
		return 'fallen'
		
	if(Utility.body_on_wall(player)):
		impact2_sound.play()
		player.velocity.x = -player.velocity.x
		player.facing_left = !player.facing_left
		sprite.set_flip_h(player.facing_left)
	
	player.velocity.x -= friction * sign(player.velocity.x)
	
	return null
