extends Label

const DELAY = 30
var time = 0

func _process(delta):
	if(time < DELAY):
		time += 1
	else:
		time = 0
		if(self.visible):
			self.visible = false
		else:
			self.visible = true
