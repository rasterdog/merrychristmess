extends CanvasLayer

onready var player = $'../../World/Player'
onready var filter = $Filter

var intensity = 2.5

func _process(delta):
	filter.get_material().set_shader_param("intensity", intensity)
