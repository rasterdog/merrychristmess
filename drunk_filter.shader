shader_type canvas_item;

uniform vec2 viewport_size;
uniform float intensity;

uniform float gradient_thickness;

void fragment() {
	vec2 screen_position = SCREEN_UV * viewport_size;
	
	vec2 variation = vec2(0.5, 0.5) * viewport_size - screen_position;
	float gap = length(variation);
	
	float zoom_intensity = gap * intensity / 1250.0;
	
	float distortion_amplitude = 0.005 * intensity;
	
	vec2 offset = 0.5 * variation / viewport_size * zoom_intensity;
	offset.x += distortion_amplitude * sin(5.0 * (TIME + 5.0 * offset.y));
	COLOR = mix(texture(SCREEN_TEXTURE, SCREEN_UV + offset), -vec4(1.0), zoom_intensity * 0.5);
}