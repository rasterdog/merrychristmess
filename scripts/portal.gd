extends Area2D

onready var player = $'/root/Game/World/Player'
onready var player_camera = player.get_node('Camera2D')
onready var fixed_camera = $'../Camera2D'
onready var player_states = player.get_node('States')
onready var moving_state = player_states.get_node('Moving')
onready var sprite = $Sprite

onready var sibling = get_parent().get_child(0) if (get_parent().get_child(0) != self) else get_parent().get_child(1)

export var portal_direction : int

var in_use = false

func _ready():
	if(portal_direction == -1):
		sprite.rotate(PI)

func use():
	in_use = true
	player.portal_ref = sibling
	player.set_position(sibling.get_position() + Vector2(0, -9))
	player.velocity.x = 0
	
	if(portal_direction == 1):
		fixed_camera.current = true
	else:
		player_camera.current = true

func _process(delta):
	for body in get_overlapping_bodies() :
		if(body == player && player_states.state == moving_state):
			sprite.visible = true
			return
	sprite.visible = false

func _on_Portal_body_entered(body):
	if(body == player):
		player.portal_ref = self

func _on_Portal_body_exited(body):
	if(!in_use && body == player):
		player.portal_ref = null
	in_use = false
