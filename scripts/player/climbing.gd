extends Node

onready var player = $"../../"

export var friction : float

var slope : float

func enter():
	
	player.velocity.y = 0
	slope = -sign(player.velocity.x)

func exit():
	pass

func process():
	pass

func physics_process():
	if(Utility.body_on_slope(player)):
		
		var climbing_input = 'move_right' if(slope < 0) else 'move_left'
		
		if(abs(player.velocity.x) <= friction || !Input.is_action_pressed(climbing_input)):
			$"../Tumble".slope = slope
			return 'tumble'
		
		player.velocity.x -= friction * sign(player.velocity.x)
	else:
		player.velocity.x = -slope * 100
		return 'moving'
	
	return null
