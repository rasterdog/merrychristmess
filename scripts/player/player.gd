extends KinematicBody2D

onready var filter_layer = $'/root/Game/FilterLayer'

export var min_dizziness : float
export var max_dizziness : float
export var dizziness_increment_divisor : float
export var dizziness_decrement : float

var velocity = Vector2(0, 0)
var facing_left = true

var portal_ref : Area2D

func _process(delta):
	
	var dizziness_increment = abs(velocity.x) / dizziness_increment_divisor
	if(dizziness_increment > 0):
		filter_layer.intensity += dizziness_increment
	else:
		filter_layer.intensity -= dizziness_decrement
	filter_layer.intensity = max(min(filter_layer.intensity, max_dizziness), min_dizziness)

func _physics_process(delta):
	move_and_slide(velocity, Utility.UP_VECTOR, false, 4, 0)
