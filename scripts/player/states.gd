extends Node

onready var states = {
	'moving': $Moving,
	'falling': $Falling,
	'tumble': $Tumble,
	'rolling': $Rolling,
	'rolling up': $RollingUp,
	'jumping': $Jumping,
	'fallen': $Fallen,
	'get up': $GetUp,
	'climbing': $Climbing,
	'final': $Final
}

var state: Object

func _ready():
	state = states['moving']

func _process(delta):
	state.process()

func _physics_process(delta):
	var state_name = state.physics_process()
	if (state_name):
		change_state(state_name)

func change_state(state_name):
	state.exit()
	state = states[state_name]
	state.enter()
