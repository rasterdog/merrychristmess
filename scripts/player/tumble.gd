extends Node

onready var player = $"../../"
onready var sprite = $"../../AnimatedSprite"

export var tumble_acceleration : float

var slope : float

func enter():
	
	if (player.facing_left && slope > 0) || (!player.facing_left && slope < 0):
		sprite.play('reverse rolling')
	else:
		sprite.play('rolling')
		
	player.velocity.x = player.velocity.y * slope / 2
	player.velocity.y = abs(player.velocity.x) + 1
	
func exit():
	sprite.set_speed_scale(1)

func process():
	sprite.set_speed_scale(abs(player.velocity.x) / 50)

func physics_process():
	
	if(player.is_on_floor()):
		return 'rolling'
		
	var slope = Utility.body_on_slope(player)
	if(!slope):
		return 'falling'
	
	player.velocity.x += tumble_acceleration * slope
	player.velocity.y = abs(player.velocity.x) * 2
	
	return null
