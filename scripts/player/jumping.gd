extends Node

onready var player = $"../../"
onready var sprite = $"../../AnimatedSprite"
onready var jumpSound = $'/root/Game/StreamPlayer/Jump'
onready var impact2_sound = $'/root/Game/StreamPlayer/Impact2'

export var rise_speed : float
export var rise_deceleration : float
export var max_hold : float
export var min_impact_speed : float

var hold : float

func enter():
	
	sprite.play('jumping')
	jumpSound.play()
	hold = 0
	
func exit():
	pass

func process():
	pass

func physics_process():
	
	if(Utility.body_on_wall(player) && abs(player.velocity.x) > min_impact_speed):
		impact2_sound.play()
		player.velocity.x = -player.velocity.x
		player.velocity.y = 0
		return 'falling'
	
	if(player.is_on_ceiling()):
		player.velocity.y = 0
		return 'falling'
	
	if(Input.is_action_pressed("jump") && hold < max_hold):
		player.velocity.y = -rise_speed
		hold += 1
	else:
		player.velocity.y += rise_deceleration
		hold = max_hold
		
		if(player.velocity.y >= 0):
			player.velocity.y = 0
			return 'falling'
	
	return null
