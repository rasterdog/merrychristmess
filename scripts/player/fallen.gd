extends Node

onready var player = $"../../"
onready var sprite = $"../../AnimatedSprite"

export var friction : float
export var min_recovery_speed : float
export var delay : float

var time = 0

func enter():
	if (player.facing_left && player.velocity.x > 0) || (!player.facing_left && player.velocity.x < 0):
		sprite.play('reverse fallen')
	else:
		sprite.play('fallen')
	time = 0
	
func exit():
	pass

func process():
	pass

func physics_process():
	
	var velocity_decrement = friction * sign(player.velocity.x)
	
	if(!player.is_on_floor()):
		return 'falling'
	
	if(abs(player.velocity.x) <= friction):
		if(time >= delay):
			return 'get up'
		player.velocity.x = 0
		time += 1
	else:
		player.velocity.x -= velocity_decrement
	
	return null
