extends Area2D

onready var end_screen = preload('res://scenes/EndScreen.tscn')

onready var player = $'/root/Game/World/Player'
onready var player_states = player.get_node('States')

func _on_End_body_entered(body):
	if(body == player):
		_end_game()
		
func _end_game():
	
	player_states.change_state('final')
	$'/root/Game'.add_child(end_screen.instance())
